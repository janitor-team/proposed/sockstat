Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: sockstat
Upstream-Contact: William Pitcock <nenolod@dereferenced.org>
Source: http://nenolod.net/sockstat

Files: *
Copyright: 2002 Dag-Erling Coïdan Smørgrav
           2008 William Pitcock <nenolod@dereferenced.org>
License: BSD-3-Clause

Files: debian/*
Copyright: 2008 William Pitcock <nenolod@dereferenced.org>
           2009 Ying-Chun Liu (PaulLiu) <grandpaul@gmail.com>
           2016 Joao Eriberto Mota Filho <eriberto@debian.org>
           2018 Thorsten Alteholz <debian@alteholz.de>
License: BSD-3-Clause

License: BSD-3-Clause
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions
 are met:
 .
    1. Redistributions of source code must retain the above copyright
       notice, this list of conditions and the following disclaimer
       in this position and unchanged.
    2. Redistributions in binary form must reproduce the above copyright
       notice, this list of conditions and the following disclaimer in the
       documentation and/or other materials provided with the distribution.
    3. The name of the author may not be used to endorse or promote products
       derived from this software without specific prior written permission.
 .
 THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
 INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
